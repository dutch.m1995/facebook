import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeColumnRightComponent } from './home-column-right.component';

describe('HomeColumnRightComponent', () => {
  let component: HomeColumnRightComponent;
  let fixture: ComponentFixture<HomeColumnRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeColumnRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeColumnRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
