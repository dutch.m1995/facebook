import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {
  
  Prinfobj={
    "Graduate":"Studied at Christ College of Engineering and Technology",
    "School":"Went to Amalorpavam Higher Secondary School",
    "Place":"Lives in Puduchcheri, Puducherry,India",
    "Location":"From Pondichéry, Puducherry,India",
    "Status":"Single",
    "Join":"Joined April 2012",
    "Friendimg":"https://media3.s-nbcnews.com/j/newscms/2019_41/3047866/191010-japan-stalker-mc-1121_06b4c20bbf96a51dc8663f334404a899.fit-2000w.JPG",
    "Myimage":"assets/cr7.jpg"

  }

  constructor() { }

  ngOnInit(): void {
  }

}
